/*
 * @Author: 炫佑科技
 * @Date: 2022-05-26 15:47:34
 * @LastEditTime: 2022-07-08 15:14:17
 * @LastEditors: 小轩
 * @Description: 
 * 炫佑科技-www.sdxuanyou.com
 */
// var BASE = "http://zz.sdxuanyou.com/api"
var BASE = "http://120.48.53.209:801"

function requestMethod(url, data, method, header, isPopup) {
    if (uni.getStorageSync('token')) {
        header.token = uni.getStorageSync('token')
    }
    return new Promise((resolve, reject) => {
        uni.request({
            url:  url,
            data,
            header: header,
            method: method,
            success: (res) => {
                uni.hideLoading()
                if (res.data.code == 200) {
                    resolve(res.data)
                } else if (res.data.code == 401 || res.data.code == 101) {
                    uni.showModal({
                        title: '登录',
                        content: '请重新登录后操作',
                        success: function(res) {
                            uni.reLaunch({
                                url: '/pages/login/login'
                                // url: '/pages/mine/mine'
                            })
                        }
                    });
                } else if (res.data.code == 400) {


                    reject(res.data)
                } else {
                    if (isPopup) {
                        reject(res.data)
                        return
                    }
                    uni.showModal({
                        title: res.data.msg
                    })
                }

            },
            fail: (err) => {
                uni.hideLoading()
            }
        })
    })
}

// loadData() {
// 	let data = {
// 		account: '19155628183',
// 		passwd: '123456'
//  }
// 	this.$http.postReq('/admin/login/login', data).then(res => {
// 		console.log(res, 777777)
// 	}).catch(err => {
// 		console.log(err, 88888)
// 	})
// }

function postReq(url, data, isPopup) {
    let header = {
        "content-type": 'application/x-www-form-urlencoded'
    }
    return requestMethod(url, data, "POST", header, isPopup)
}

function getReq(url, data, isPopup) {
    let header = {
		"content-type": 'application/x-www-form-urlencoded'
    }
    return requestMethod(url, data, "GET", header, isPopup)
}


function random(url, data, isPopup) {
    let header = {
        "content-type": 'application/x-www-form-urlencoded'
    }
    return requestMethod(url, data, "GET", header, isPopup)
}

//随机下单
function createOrder(url, data, isPopup) {
    let header = {
        "content-type": 'application/x-www-form-urlencoded',
		"token" : uni.getStorageSync('token')
    }
    return requestMethod(url, data, "POST", header, isPopup)
}

export default {
    postReq,
    getReq,
	random,
	createOrder
}