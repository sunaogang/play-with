/*
 * @Author: 炫佑科技
 * @Date: 2022-07-11 09:15:15
 * @LastEditTime: 2022-07-11 10:14:36
 * @LastEditors: 小轩
 * @Description: 
 * 炫佑科技-www.sdxuanyou.com
 */
import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import http from "request/ajax.js"
Vue.prototype.$http = http
    // 让页面的 onLoad 在 onLaunch 之后执行
Vue.prototype.$onLaunched = new Promise(resolve => {
    Vue.prototype.$isResolve = resolve
})
Vue.prototype.$getUrlCode = function(url) {
    var pattern = /(\w+)=(\w+)/ig; //定义正则表达式

    var parames = {}; //定义数组

    url.replace(pattern, function(a, b, c) {
        parames[b] = c;
    });
    return parames.code; //返回这个数组.
}
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
    // #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
    const app = createSSRApp(App)
    return {
        app
    }
}
// #endif